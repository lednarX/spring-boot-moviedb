package com.example.springboot.Service;

import com.example.springboot.integration.MovieIntegration;
import com.example.springboot.model.Movie;

import java.util.List;

public class MovieService {
    private MovieIntegration movieIntegration = new MovieIntegration();

    public List<Movie> getPopularMovies(){
        return movieIntegration.getPopularMovies();
    }

    public Movie getMovieById(String movieId){
        return movieIntegration.getMovieById( movieId );
    }
}
