package com.example.springboot.controller;

import com.example.springboot.model.Movie;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public interface MovieController {

	@GetMapping("/popularMovie")
	List<Movie> getPopularMovies();

	@GetMapping("/movie/{movieId}")
	Movie getMovieById(@PathVariable String movieId);


}