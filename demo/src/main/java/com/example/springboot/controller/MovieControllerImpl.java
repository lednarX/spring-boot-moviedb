package com.example.springboot.controller;

import com.example.springboot.Service.MovieService;
import com.example.springboot.model.Movie;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MovieControllerImpl implements MovieController
{
    private MovieService movieService = new MovieService();

    public List<Movie> getPopularMovies(){
        return movieService.getPopularMovies();
    }

    public Movie getMovieById(String movieId){
        return movieService.getMovieById( movieId );
    }
}