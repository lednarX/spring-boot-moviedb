package com.example.springboot.integration;

import com.example.springboot.model.Movie;
import com.example.springboot.utils.MovieUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class MovieIntegration {

    private String API_KEY = "5ebd86d28552a95299d66450a293185d";

    /*
    * Get the list of popular movies from the MovieDB.
    * @return List<Movie> a list of popular movies with the details
     */
    public List<Movie> getPopularMovies()
    {
        List<Movie> popularMovies = new ArrayList<>();
        try {
            String output;
            JSONObject movieObj;
            JSONArray movieArray;

            String urlPopular = "https://api.themoviedb.org/3/movie/popular?api_key="+API_KEY;
            URL url = new URL( urlPopular );
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");

            BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
            System.out.println("Output from MovieDB .... \n");
            while ((output = br.readLine()) != null) {
                movieObj = new JSONObject( output );
                movieArray = movieObj.getJSONArray( MovieUtils.KEY_RESULTS );
                for(int i=0; i<movieArray.length(); i++ )
                {
                    popularMovies.add( MovieUtils.parse( movieArray.get(i).toString() ) );
                }
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return popularMovies;
    }

    /*
     * Get the movie with the id specified from the MovieDB.
     * @param movieId - movie id
     * @return Movie - the movie with the details
     */
    public Movie getMovieById(String movieId)
    {
        try {
            String output;

            String urlPopular = "https://api.themoviedb.org/3/movie/"+movieId+"?api_key="+API_KEY;
            URL url = new URL( urlPopular );
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");

            BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
            System.out.println("Output from MovieDB .... \n");
            while ((output = br.readLine()) != null) {
                return MovieUtils.parse( output );
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
