package com.example.springboot.model;

import java.io.Serializable;

/*
* This is the model class for the Movie
 */
public class Movie implements Serializable {
    private int movieId;
    private String movieTitle;
    private String movieOverview;
    private String moviePoster;
    private float movieAverage;
    private String movieReleaseDate;

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getMovieOverview() {
        return movieOverview;
    }

    public void setMovieOverview(String movieOverview) {
        this.movieOverview = movieOverview;
    }

    public String getMoviePoster() {
        return moviePoster;
    }

    public void setMoviePoster(String moviePoster) {
        this.moviePoster = moviePoster;
    }

    public float getMovieAverage() {
        return movieAverage;
    }

    public void setMovieAverage(float movieAverage) {
        this.movieAverage = movieAverage;
    }

    public String getMovieReleaseDate() {
        return movieReleaseDate;
    }

    public void setMovieReleaseDate(String movieReleaseDate) {
        this.movieReleaseDate = movieReleaseDate;
    }
}
