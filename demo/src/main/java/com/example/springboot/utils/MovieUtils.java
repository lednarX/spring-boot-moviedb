package com.example.springboot.utils;

import com.example.springboot.model.Movie;

import org.json.JSONException;
import org.json.JSONObject;

public class MovieUtils
{
    public static final String KEY_RESULTS = "results";
    public static final String KEY_ID = "id";
    public static final String KEY_VOTE = "vote_average";
    public static final String KEY_TITLE = "title";
    public static final String KEY_OVERVIEW = "overview";
    public static final String KEY_POSTER = "poster_path";
    public static final String KEY_RELEASE_DATE = "release_date";

    public static Movie parse(String output)
    {
        Movie movie = new Movie();
        try {
            JSONObject obj = new JSONObject( output );
            movie.setMovieId( obj.getInt(KEY_ID) );
            movie.setMovieAverage( obj.getInt(KEY_VOTE) );
            movie.setMovieTitle( obj.getString(KEY_TITLE) );
            movie.setMovieOverview( obj.getString(KEY_OVERVIEW) );
            movie.setMoviePoster( obj.getString(KEY_POSTER) );
            movie.setMovieReleaseDate( obj.getString(KEY_RELEASE_DATE) );
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return movie;
    }
}
